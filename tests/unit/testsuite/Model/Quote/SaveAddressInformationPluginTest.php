<?php


namespace Perspective\NovaposhtaShipping\tests\unit\testsuite\Model\Quote;

use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Backend\Model\View\Result\Forward;
use Perspective\NovaposhtaCatalog\Model\CityRepository;
use Perspective\NovaposhtaShipping\Model\ShippingWarehouse;
use Perspective\NovaposhtaShipping\Model\ShippingCheckoutAddressFactory;
use Perspective\NovaposhtaShipping\Model\Quote\SaveAddressInformationPlugin as TestClass;
use Perspective\NovaposhtaShipping\tests\unit\TestHelpers\MageObjMan;

/**
 * Class SaveAddressInformationPluginTest
 * Save data on checkout
 */
class SaveAddressInformationPluginTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    public $objMan;
    /**
     * @var TestClass
     */
    public $testClass;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $resultForwardFactoryMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $resultForwardMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockBuilder
     */
    public $shippingCheckoutAddressFactoryMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $shippingCheckoutAddress;
    public $requestMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $sessionMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $quoteMock;
    public $contextRealObj;
    /**
     * @var \Perspective\NovaposhtaShipping\tests\unit\TestHelpers\MageObjMan
     */
    public $realObjMan;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $cityRepositoryMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $cityModelMock;

    public function setUp()
    {
        $this->objMan = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->resultForwardFactoryMock = $this->getMockBuilder(ForwardFactory::class)
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->shippingCheckoutAddressFactoryMock = $this
            ->getMockBuilder(ShippingCheckoutAddressFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        //   $this->shippingCheckoutAddress = $this->createMock(ShippingCheckoutAddress::class);
        $this->shippingCheckoutAddress = $this->objMan->getObject(ShippingWarehouse::class);
        // $this->shippingCheckoutAddress->method('setCartId')->will();
        $this->shippingCheckoutAddressFactoryMock->method('create')->willReturn($this->shippingCheckoutAddress);
        $this->resultForwardMock = $this->createMock(Forward::class);
        $this->resultForwardFactoryMock->method('create')->willReturn($this->resultForwardMock);
        $this->requestMock = $this->createMock(\Magento\Framework\App\Request\Http::class);
        $this->requestMock->method('getParam')->withConsecutive(['novaposhta_city'], ['novaposhta_warehouse'])
            ->willReturnOnConsecutiveCalls('71', '92');
        $this->quoteMock = $this->createMock(\Magento\Quote\Model\Quote::class);
        $this->quoteMock->method('getId')->willReturn(1);
        $this->sessionMock = $this->getMockBuilder(\Magento\Backend\Model\Session\Quote::class)
            ->disableOriginalConstructor()->setMethods(['_getQuote'])->getMock();
        $this->sessionMock->method('_getQuote')->willReturn($this->quoteMock);
        $this->realObjMan = new MageObjMan();
        $this->cityRepositoryMock = $this->getMockBuilder(CityRepository::class)
            ->setMethods(['getCityById'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->cityModelMock = $this->getMockBuilder(\Perspective\NovaposhtaCatalog\Model\City\City::class)
            ->setMethods(['getCityID'])->disableOriginalConstructor()->getMock();
        $this->cityModelMock->method('getCityID')->willReturn((int)$this->requestMock->getParam('novaposhta_city'));
        $this->cityRepositoryMock->method('getCityById')->willReturn($this->cityModelMock);
        $this->contextRealObj = $this->realObjMan->objectManager->create(\Magento\Backend\App\Action\Context::class);
        $this->testClass = $this->objMan->getObject(
            TestClass::class,
            [
                'resultForwardFactory' => $this->resultForwardFactoryMock,
                'shippingCheckoutAddressFactory' => $this->shippingCheckoutAddressFactoryMock,
                'request' => $this->requestMock,
                'context' => $this->contextRealObj,
                'cityRepository'=>$this->cityRepositoryMock,
                'cartId' => 1
            ]
        );
    }

    public function testBeforeSaveAddressInformation()
    {
        $subject = $this->createMock(\Magento\Checkout\Model\ShippingInformationManagement::class);
        $extensionAttr = $this->createMock(\Magento\Quote\Api\Data\AddressExtensionInterface::class);
        $extensionAttr->method('getPerspectiveNovaposhtaShippingWarehouse')->willReturn('92');
        $extensionAttr->method('getPerspectiveNovaposhtaShippingCity')->willReturn('71');
        $quoteAddress = $this->createMock(\Magento\Quote\Model\Quote\Address::class);
        $quoteAddress->method('getExtensionAttributes')->willReturn($extensionAttr);
        $addressInformation = $this->createMock(\Magento\Checkout\Model\ShippingInformation::class);
        $addressInformation->method('getShippingAddress')->willReturn($quoteAddress);
        $this->testClass->beforeSaveAddressInformation($subject, 1, $addressInformation);
        $reflectionClass = new \ReflectionClass(get_class($this->testClass));
        $p = $reflectionClass->getProperty('checkoutNovaposhtaModel');
        $p->setAccessible(true);
        $checkoutModel = $p->getValue($this->testClass);
        $this->assertNotNull($checkoutModel->getData());
        $this->assertArrayHasKey('cart_id', $checkoutModel->getData());
        $this->assertArrayHasKey('city_id', $checkoutModel->getData());
        $this->assertArrayHasKey('warehouse_id', $checkoutModel->getData());
        $this->assertEquals(1, $checkoutModel->getCartId());
        $this->assertEquals(71, $checkoutModel->getCityId());
        $this->assertEquals(92, $checkoutModel->getWarehouseId());
    }
}
