<?php


namespace Perspective\NovaposhtaShipping\tests\unit\testsuite\Model\Carrier;

use Magento\Framework\HTTP\ZendClient;
use Magento\Framework\HTTP\ZendClientFactory;
use Magento\Quote\Model\Quote\Address\RateResult\Error;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\Method;
use Magento\Shipping\Model\Rate\Result;
use Magento\Shipping\Model\Rate\ResultFactory;
use Perspective\NovaposhtaShipping\Model\Carrier\NovaposhtaShipping as TestClass;
use Perspective\NovaposhtaShipping\tests\unit\TestHelpers\MageObjMan;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;

class NovaposhtaShippingTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    public $objMan;
    /**
     * @var TestClass
     */
    public $testClass;
    public $rateErrorFactoryMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $rateErrorFactory;
    public $httpClientFactoryMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockBuilder
     */
    public $rateResultFactoryMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $rateResultMock;
    /**
     * @var \Perspective\NovaposhtaShipping\tests\unit\TestHelpers\MageObjMan
     */
    public $realObjMan;
    public $rateMethod;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $rateMethodMockFactory;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $scopeConfigMock;

    public function setUp()
    {
        $this->objMan = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->rateErrorFactory = $this->createMock(Error::class);
        $this->rateErrorFactoryMock = $this->getMockBuilder(ErrorFactory::class)
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->rateErrorFactoryMock->method('create')->willReturn($this->rateErrorFactory);
        $httpClient = $this->createMock(ZendClient::class);
        $this->httpClientFactoryMock = $this->getMockBuilder(ZendClientFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $this->httpClientFactoryMock->method('create')->willReturn($httpClient);
        $this->rateResultMock = $this->createMock(Result::class);
        $this->rateResultMock->method('append')->willReturn($this);
        $this->rateResultFactoryMock = $this->getMockBuilder(ResultFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $this->rateResultFactoryMock->method('create')->willReturn($this->rateResultMock);
        $this->realObjMan = new MageObjMan();
        $this->rateMethod = $this->realObjMan->objectManager->create(Method::class);
        $this->rateMethodMockFactory = $this->getMockBuilder(MethodFactory::class)->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $this->rateMethodMockFactory->method('create')->willReturn($this->rateMethod);
        $this->scopeConfigMock = $this->getMockBuilder(\Magento\Framework\App\Config::class)
            ->disableOriginalConstructor()
            ->setMethods(['isSetFlag', 'getValue'])
            ->getMock();
        $this->scopeConfigMock->method('isSetFlag')->with(
            'carriers/novaposhtashipping/active',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            null
        )->willReturn(true);
        $this->scopeConfigMock->method('getValue')->withConsecutive(
            [
                'carriers/novaposhtashipping/price',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                null
            ],
            [
                'carriers/novaposhtashipping/title',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                null
            ],
            [
                'carriers/novaposhtashipping/name',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                null
            ]
        )->willReturnOnConsecutiveCalls('50', 'testTitle', 'testName');
        $this->testClass = $this->objMan->getObject(
            TestClass::class,
            [
                'rateErrorFactory' => $this->rateErrorFactoryMock,
                'httpClientFactory' => $this->httpClientFactoryMock,
                'rateResultFactory' => $this->rateResultFactoryMock,
                'rateMethodFactory' => $this->rateMethodMockFactory,
                'scopeConfig' => $this->scopeConfigMock
            ]
        );
    }

    public function testCollectRates()
    {
        $rateRequestMock = $this->createMock(\Magento\Quote\Model\Quote\Address\RateRequest::class);
        $this->testClass->collectRates($rateRequestMock);
        $reflectionClass = new \ReflectionClass(get_class($this->testClass));
        $p = $reflectionClass->getProperty('method');
        $p->setAccessible(true);
        $shippingMethod = $p->getValue($this->testClass);
        $this->assertArrayHasKey('carrier', $shippingMethod->getData());
        $this->assertArrayHasKey('carrier_title', $shippingMethod->getData());
        $this->assertArrayHasKey('method', $shippingMethod->getData());
        $this->assertArrayHasKey('method_title', $shippingMethod->getData());
        $this->assertArrayHasKey('price', $shippingMethod->getData());
        $this->assertArrayHasKey('cost', $shippingMethod->getData());
        $this->assertNotEmpty($shippingMethod->getCarrier());
        $this->assertNotEmpty($shippingMethod->getCarrierTitle());
        $this->assertNotEmpty($shippingMethod->getMethod());
        $this->assertNotEmpty($shippingMethod->getMethodTitle());
        $this->assertNotEmpty($shippingMethod->getPrice());
        $this->assertNotEmpty($shippingMethod->getCost());
        $this->assertEquals('novaposhtashipping', $shippingMethod->getCarrier());
        $this->assertEquals('testTitle', $shippingMethod->getCarrierTitle());
        $this->assertEquals('novaposhtashipping', $shippingMethod->getMethod());
        $this->assertEquals('testName', $shippingMethod->getMethodTitle());
        $this->assertEquals(50.0, $shippingMethod->getPrice());
        $this->assertEquals(50, $shippingMethod->getCost());
    }
}
