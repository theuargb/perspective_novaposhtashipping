<?php


namespace Perspective\NovaposhtaShipping\tests\unit\testsuite\Controller\Ajax;

use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Serialize\Serializer\Json;
use Perspective\NovaposhtaCatalog\Api\CityRepositoryInterface;
use Perspective\NovaposhtaCatalog\Api\WarehouseRepositoryInterface;
use Perspective\NovaposhtaShipping\Controller\Ajax\Warehouse as TestClass;
use Perspective\NovaposhtaShipping\tests\unit\TestHelpers\FactoryHelper;
use Perspective\NovaposhtaShipping\tests\unit\TestHelpers\FactoryMockHelper;

class WarehouseTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    public $objMan;
    /**
     * @var TestClass
     */
    public $testClass;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $contextMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $resolverMock;
    /**
     * @var object
     */
    public $modelAddressRealObj;
    /**
     * @var \Perspective\NovaposhtaShipping\tests\unit\TestHelpers\MageObjMan
     */
    public $mageObjMan;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $customerSessionMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $customerModelMock;
    public $warehouseRepositoryRealObj;
    /**
     * @var object
     */
    public $jsonSerializerObj;
    /**
     * @var object
     */
    public $resultJsonFactoryMock;
    public $cityRepositoryRealObj;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $requestMock;

    public function setUp()
    {
        $this->objMan = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->mageObjMan = new \Perspective\NovaposhtaShipping\tests\unit\TestHelpers\MageObjMan();
        $this->contextMock = $this->createMock(\Magento\Framework\App\Action\Context::class);
        $this->modelAddressRealObj = $this->mageObjMan->objectManager->create(\Magento\Customer\Model\Address::class);
        $this->resolverMock = $this->getMockBuilder(\Magento\Framework\Locale\Resolver::class)
            ->disableOriginalConstructor()
            ->setMethods(['getLocale'])
            ->getMock();
        $this->resolverMock->method('getLocale')->willReturn('uk_UA');

        $this->customerSessionMock = $this->getMockBuilder(\Magento\Customer\Model\Session::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->customerModelMock = $this->getMockBuilder(\Magento\Customer\Model\Customer::class)
            ->disableOriginalConstructor()
            ->setMethods(['getDefaultBilling'])
            ->getMock();
        $this->customerModelMock->method('getDefaultBilling')->willReturn(1);
        $this->customerSessionMock->method('getCustomer')->willReturn($this->customerModelMock);
        $this->warehouseRepositoryRealObj = $this->mageObjMan->objectManager
            ->create(WarehouseRepositoryInterface::class);
        $this->cityRepositoryRealObj = $this->mageObjMan->objectManager->create(CityRepositoryInterface::class);
        $this->jsonSerializerObj = $this->objMan->getObject(Json::class);
        $this->resultJsonFactoryMock = $this->createMock(JsonFactory::class);
        $resultJsonMock = $this->createMock(\Magento\Framework\Controller\Result\Json::class);
        $resultJsonMock->method('setData')->willReturn($this);
        $this->requestMock = $this->createMock(\Magento\Framework\App\Request\Http::class);
        $this->requestMock->method('getParam')->with('cityId')->willReturn('71');
        $this->resultJsonFactoryMock->method('create')->willReturn($resultJsonMock);
        $this->testClass = $this->objMan->getObject(
            TestClass::class,
            [
                'resolver' => $this->resolverMock,
                'modelAddress' => $this->modelAddressRealObj,
                'customerSession' => $this->customerSessionMock,
                'warehouseRepository' => $this->warehouseRepositoryRealObj,
                'jsonSerializer' => $this->jsonSerializerObj,
                'cityRepository' => $this->cityRepositoryRealObj,
                'resultJsonFactory' => $this->resultJsonFactoryMock,
                'request' => $this->requestMock
            ]
        );
    }

    public function testExecute()
    {
        $testClass = $this->testClass->execute();
        $resultDecodedJson = $this->jsonSerializerObj->unserialize($this->testClass->serializedJsonString);
        $this->assertArrayHasKey('warehouseList', $resultDecodedJson);
        $this->assertArrayHasKey('customerData', $resultDecodedJson);
        $this->assertArrayHasKey('label', $resultDecodedJson['warehouseList'][0]);
        $this->assertArrayHasKey('value', $resultDecodedJson['warehouseList'][0]);
        $this->assertNotEmpty($resultDecodedJson['warehouseList'][0]['label']);
        $this->assertNotNull($resultDecodedJson['warehouseList'][0]['value']);
        $this->assertNotEmpty($resultDecodedJson['customerData']['entity_id']);
        $this->assertNotEmpty($resultDecodedJson['customerData']['city']);
    }
}
