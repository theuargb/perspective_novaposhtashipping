<?php


namespace Perspective\NovaposhtaShipping\tests\unit\testsuite\Controller\Adminhtml\Ajax;

use Perspective\NovaposhtaCatalog\Api\CityRepositoryInterface;
use Perspective\NovaposhtaShipping\Controller\Adminhtml\Ajax\City as TestClass;
use Perspective\NovaposhtaShipping\tests\unit\TestHelpers\FactoryHelper;
use Perspective\NovaposhtaShipping\tests\unit\TestHelpers\FactoryMockHelper;

class CityTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    public $objMan;
    /**
     * @var TestClass
     */
    public $testClass;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $contextMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $resolverMock;
    /**
     * @var object
     */
    public $modelAddressRealObj;
    /**
     * @var \Perspective\NovaposhtaShipping\tests\unit\TestHelpers\MageObjMan
     */
    public $mageObjMan;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $customerSessionMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $customerModelMock;
    public $cityRepositoryRealObj;
    /**
     * @var object
     */
    public $jsonSerializerObj;
    /**
     * @var object
     */
    public $resultJsonFactoryMock;

    public function setUp()
    {
        $this->objMan = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->mageObjMan = new \Perspective\NovaposhtaShipping\tests\unit\TestHelpers\MageObjMan();
        $this->contextMock = $this->createMock(\Magento\Framework\App\Action\Context::class);
        $this->modelAddressRealObj = $this->mageObjMan->objectManager->create(\Magento\Customer\Model\Address::class);
        $this->resolverMock = $this->getMockBuilder(\Magento\Framework\Locale\Resolver::class)
            ->disableOriginalConstructor()
            ->setMethods(['getLocale'])
            ->getMock();
        $this->resolverMock->method('getLocale')->willReturn('uk_UA');

        $this->customerSessionMock = $this->getMockBuilder(\Magento\Customer\Model\Session::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->customerModelMock = $this->getMockBuilder(\Magento\Customer\Model\Customer::class)
            ->disableOriginalConstructor()
            ->setMethods(['getDefaultBilling'])
            ->getMock();
        $this->customerModelMock->method('getDefaultBilling')->willReturn(1);
        $this->customerSessionMock->method('getCustomer')->willReturn($this->customerModelMock);
        $this->cityRepositoryRealObj = $this->mageObjMan->objectManager->create(CityRepositoryInterface::class);
        $this->jsonSerializerObj = $this->objMan->getObject(\Magento\Framework\Serialize\Serializer\Json::class);
        $this->resultJsonFactoryMock = $this->createMock(\Magento\Framework\Controller\Result\JsonFactory::class);
        $resultJsonMock = $this->createMock(\Magento\Framework\Controller\Result\Json::class);
        $resultJsonMock->method('setData')->willReturn($this);
        $this->resultJsonFactoryMock->method('create')->willReturn($resultJsonMock);
        $this->testClass = $this->objMan->getObject(
            TestClass::class,
            [
                'resolver' => $this->resolverMock,
                'modelAddress' => $this->modelAddressRealObj,
                'customerSession' => $this->customerSessionMock,
                'cityRepository' => $this->cityRepositoryRealObj,
                'jsonSerializer' => $this->jsonSerializerObj,
                'resultJsonFactory' => $this->resultJsonFactoryMock
            ]
        );
    }

    public function testExecute()
    {
        $testClass = $this->testClass->execute();
        $resultDecodedJson = $this->jsonSerializerObj->unserialize($this->testClass->serializedJsonString);
        $this->assertArrayHasKey('cityList', $resultDecodedJson);
        $this->assertArrayHasKey('customerData', $resultDecodedJson);
        $this->assertArrayHasKey('label', $resultDecodedJson['cityList'][0]);
        $this->assertArrayHasKey('value', $resultDecodedJson['cityList'][0]);
        $this->assertNotEmpty($resultDecodedJson['cityList'][0]['label']);
        $this->assertNotNull($resultDecodedJson['cityList'][0]['value']);
        $this->assertNotEmpty($resultDecodedJson['customerData']['entity_id']);
        $this->assertNotEmpty($resultDecodedJson['customerData']['city']);
    }
}
