<?php


namespace Perspective\NovaposhtaShipping\tests\unit\testsuite\Block\Adminhtml\Order;

use Perspective\NovaposhtaCatalog\Api\WarehouseRepositoryInterface;
use Perspective\NovaposhtaCatalog\Model\Warehouse\Warehouse;
use Perspective\NovaposhtaCatalog\Model\WarehouseRepository;
use Perspective\NovaposhtaShipping\Block\Adminhtml\Order\NovaposhtaDeliveryInfo as TestClass;
use Perspective\NovaposhtaShipping\tests\unit\TestHelpers\MageObjMan;
use const Perspective\NovaposhtaShipping\Model\ResourceModel\ShippingWarehouse\Collection;

class NovaposhtaDeliveryInfoTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    public $objMan;
    /**
     * @var TestClass
     */
    public $testClass;
    /**
     * @var \Perspective\NovaposhtaShipping\tests\unit\TestHelpers\MageObjMan
     */
    public $realObjMan;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $contextMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $httpMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $orderRepositoryMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $orderModelMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $scopeConfigMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $shippingCheckoutAddressResourceModelCollectionMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $shippingCheckoutAddressModelMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockBuilder
     */
    public $warehouseRepositoryMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $warehouseModelMock;

    public function setUp()
    {
        $this->objMan = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->realObjMan = new MageObjMan();
        $this->orderRepositoryMock = $this->getMockBuilder(\Magento\Sales\Model\OrderRepository::class)
            ->setMethods(['get'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->orderModelMock = $this->getMockBuilder(\Magento\Sales\Model\Order::class)
            ->setMethods(['getStoreId'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->scopeConfigMock = $this->getMockBuilder(\Magento\Framework\App\Config::class)
            ->setMethods(['getValue'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->orderModelMock->method('getStoreId')->willReturn('1');
        $this->orderRepositoryMock->method('get')->willReturn($this->orderModelMock);
        $this->scopeConfigMock->method('getValue')
            ->withConsecutive(
                [
                    'general/locale/code',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                    intval($this->orderModelMock->getStoreId())
                ]
            )->willReturnOnConsecutiveCalls('uk_UA');
        $this->shippingCheckoutAddressResourceModelCollectionMock = $this
            ->getMockBuilder(
                \Perspective\NovaposhtaShipping\Model\ResourceModel\ShippingWarehouse\Collection::class
            )
            ->setMethods(['getItemByColumnValue'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->shippingCheckoutAddressModelMock = $this
            ->getMockBuilder(\Perspective\NovaposhtaShipping\Model\ShippingWarehouse::class)
            ->setMethods(['getCityId', 'getWarehouseId'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->shippingCheckoutAddressModelMock->method('getCityId')->willReturn('71');
        $this->shippingCheckoutAddressModelMock->method('getWarehouseId')->willReturn('92');
        $this->shippingCheckoutAddressResourceModelCollectionMock->method('getItemByColumnValue')
            ->withAnyParameters()->willReturn($this->shippingCheckoutAddressModelMock);
        $this->httpMock = $this->createMock(\Magento\Framework\App\Request\Http::class);
        $this->httpMock->method('getParam')->with('order_id')->willReturn(1);
        $this->contextMock = $this->getMockBuilder(\Magento\Backend\Block\Template\Context::class)
            ->setMethods(['getRequest'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->contextMock->method('getRequest')->willReturn($this->httpMock);

        $this->warehouseRepositoryMock = $this->getMockBuilder(WarehouseRepository::class)
            ->setMethods(['getWarehouseById'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->warehouseModelMock = $this->getMockBuilder(Warehouse::class)->setMethods(['getData'])
            ->disableOriginalConstructor()->getMock();
        $this->warehouseModelMock->method('getData')->willReturn('Шостка');
        $this->warehouseRepositoryMock->method('getWarehouseById')
            ->withAnyParameters()->willReturn($this->warehouseModelMock);
        $this->testClass = $this->objMan->getObject(
            TestClass::class,
            [
                'context' => $this->contextMock,
                'orderRepository' => $this->orderRepositoryMock,
                'scopeConfig' => $this->scopeConfigMock,
                'shippingCheckoutAddressResourceModelCollection' =>
                    $this->shippingCheckoutAddressResourceModelCollectionMock,
                'warehouseRepository' => $this->warehouseRepositoryMock
            ]
        );
    }

    public function testGetNovaposhtaWarehouse()
    {
        $warehouseName = $this->testClass->getNovaposhtaWarehouse();
        $this->assertNotEmpty($warehouseName);
        $this->assertEquals('Шостка', $warehouseName);
    }

    public function testGetNovaposhtaCity()
    {
        $warehouseName = $this->testClass->getNovaposhtaWarehouse();
        $this->assertNotEmpty($warehouseName);
        $this->assertEquals('Шостка', $warehouseName);
    }
}
