<?php


namespace Perspective\NovaposhtaShipping\tests\unit\testsuite\Plugin\Controller\Adminhtml;

use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Backend\Model\View\Result\Forward;
use Perspective\NovaposhtaCatalog\Model\CityRepository;
use Perspective\NovaposhtaShipping\Model\ShippingWarehouse;
use Perspective\NovaposhtaShipping\Model\ShippingCheckoutAddressFactory;
use Perspective\NovaposhtaShipping\Plugin\Controller\Adminhtml\SaveAddressInformationPlugin as TestClass;
use Perspective\NovaposhtaShipping\tests\unit\TestHelpers\MageObjMan;

/**
 * Class SaveAddressInformationPluginTest
 * Test for order save data
 */
class SaveAddressInformationPluginTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    public $objMan;
    /**
     * @var TestClass
     */
    public $testClass;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $resultForwardFactoryMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $resultForwardMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockBuilder
     */
    public $shippingCheckoutAddressFactoryMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $shippingCheckoutAddress;
    public $requestMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $sessionMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $quoteMock;
    public $contextRealObj;
    /**
     * @var \Perspective\NovaposhtaShipping\tests\unit\TestHelpers\MageObjMan
     */
    public $realObjMan;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $cityModelMock;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    public $cityRepositoryMock;

    public function setUp()
    {
        $this->objMan = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->resultForwardFactoryMock = $this->getMockBuilder(ForwardFactory::class)
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->shippingCheckoutAddressFactoryMock = $this
            ->getMockBuilder(ShippingCheckoutAddressFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        //   $this->shippingCheckoutAddress = $this->createMock(ShippingCheckoutAddress::class);
        $this->shippingCheckoutAddress = $this->objMan->getObject(ShippingWarehouse::class);
        // $this->shippingCheckoutAddress->method('setCartId')->will();
        $this->shippingCheckoutAddressFactoryMock->method('create')->willReturn($this->shippingCheckoutAddress);
        $this->resultForwardMock = $this->createMock(Forward::class);
        $this->resultForwardFactoryMock->method('create')->willReturn($this->resultForwardMock);
        $this->requestMock = $this->getMockBuilder(\Magento\Framework\App\Request\Http::class)
            ->setMethods(['getParam'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->requestMock
            ->method('getParam')
            ->withConsecutive(
                ['novaposhta_warehouse'],
                ['novaposhta_city']
            )
            ->willReturnOnConsecutiveCalls(92, 71);
        $this->quoteMock = $this->createMock(\Magento\Quote\Model\Quote::class);
        $this->quoteMock->method('getId')->willReturn(1);
        $this->sessionMock = $this->getMockBuilder(\Magento\Backend\Model\Session\Quote::class)
            ->disableOriginalConstructor()->setMethods(['_getQuote'])->getMock();
        $this->sessionMock->method('_getQuote')->willReturn($this->quoteMock);
        $this->realObjMan = new MageObjMan();
        $this->cityRepositoryMock = $this->getMockBuilder(CityRepository::class)
            ->setMethods(['getCityById'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->cityModelMock = $this->getMockBuilder(\Perspective\NovaposhtaCatalog\Model\City\City::class)
            ->setMethods(['getCityID'])->disableOriginalConstructor()->getMock();
        $this->cityModelMock->method('getCityID')->willReturn(71);
        $this->cityRepositoryMock->method('getCityById')->willReturn($this->cityModelMock);
        $this->contextRealObj = $this->realObjMan->objectManager->create(\Magento\Backend\App\Action\Context::class);
        $this->testClass = $this->objMan->getObject(
            TestClass::class,
            [
                'resultForwardFactory' => $this->resultForwardFactoryMock,
                'shippingCheckoutAddressFactory' => $this->shippingCheckoutAddressFactoryMock,
                'request' => $this->requestMock,
                'context' => $this->contextRealObj,
                'cityRepository' => $this->cityRepositoryMock,
                'cartId' => 1
            ]
        );
    }

    public function testBeforeExecute()
    {
        $subject = $this->createMock(\Magento\Sales\Controller\Adminhtml\Order\Create\Save::class);
        $this->testClass->beforeExecute($subject);
        $reflectionClass = new \ReflectionClass(get_class($this->testClass));
        $p = $reflectionClass->getProperty('checkoutNovaposhtaModel');
        $p->setAccessible(true);
        $checkoutModel = $p->getValue($this->testClass);
        $this->assertNotNull($checkoutModel->getData());
        $this->assertArrayHasKey('cart_id', $checkoutModel->getData());
        $this->assertArrayHasKey('city_id', $checkoutModel->getData());
        $this->assertArrayHasKey('warehouse_id', $checkoutModel->getData());
        $this->assertEquals(0, $checkoutModel->getCartId());
        $this->assertEquals(71, $checkoutModel->getCityId());
        $this->assertEquals(92, $checkoutModel->getWarehouseId());
    }
}
