<?php


namespace Perspective\NovaposhtaShipping\tests\unit\TestHelpers;

/**
 * Class FactoryHelper
 */
class FactoryMockHelper extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \Magento\Framework\TestFramework\Unit\Helper\ObjectManager
     */
    public $objMan;

    /**
     * @param $instanceName
     * @return $instanceName
     */
    public function getMockupFactory($instanceName)
    {
        $factory = $this->getMockBuilder($instanceName . 'Factory')
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $factory->expects($this->any())
            ->method('create')
            ->will(
                $this->returnValue($this->createMock($instanceName))
            );
        return $factory;
    }
}
