define([
    'jquery',
    'ko',
    'Magento_Ui/js/form/element/select',
    'mage/url',
    'postbox',
    'mage/translate',
    'Perspective_NovaposhtaShipping/js/lib/select2/select2'
], function ($, ko, Select, url, postbox, setShippingInformationAction) {
    'use strict';
    return Select.extend({

        defaults: {
            template: 'Perspective_NovaposhtaShipping/order/create/city',
            backendRestURL: ''
        },

        initialize: function (config) {
            this._super();
            this.backendRestURL(config.cityBackendUrl);
            return this;
        },

        initObservable: function () {
            this._super();
            this.observe('backendRestURL');
            return this;
        },

        select2: function (element) {
            var lang = "ru";
            if ($('html').attr('lang') == "uk") {
                lang = "uk";
            }
            $(element).select2({
                placeholder: $.mage.__(''),
                dropdownAutoWidth: true,
                width: '100%',
                minimumInputLength: 2,
                language: lang,
                data: [{id: 0, text: $.mage.__('Choose city')}],
                ajax: {
                    url: this.backendRestURL(),
                    type: "post",
                    dataType: 'json',
                    contentType: "application/json",
                    delay: 1000,
                    beforeSend: function (xhr, ajax) {
                        //Empty to remove magento's default handler
                    },
                    data: function (params) {
                        var query = JSON.stringify({
                            term: params.term,
                            form_key: window.FORM_KEY
                        })
                        return query;
                    },
                    processResults: function (data) {
                        return {
                            results: JSON.parse(data)
                        };
                    }
                }
            });
        },
        getPreview: function () {
            return $('[id="' + this.uid + '"] option:selected').text();
        },
        setDifferedFromDefault: function (a, b, c) {
            this._super();
            this.exportCityName(this.getPreview());
            this.exportCityValue(this.value());
        },
        exportCityName: function (value) {
            if ($('#order-shipping_same_as_billing').is(":checked")) {
                this.exportValue(value, $('#order-billing_address_city'));
            }
            if (this.index == 'cityInputAutocompleteBilling') {
                this.exportLabel(value, $('#order-billing_address_city'));
            }
            if (this.index == 'cityInputAutocompleteShipping') {
                this.exportLabel(value, $('#order-shipping_address_city'));
            }
        },
        exportCityValue: function (value) {
            if ($('#order-shipping_same_as_billing').is(":checked")) {
                this.exportValue(value, $('[name="novaposhtashipping_city_hidden"]'));
            }
            if (this.index == 'cityInputAutocompleteBilling') {
                this.exportValue(value, $('[name="novaposhtashipping_city_hidden"]'));
            }
            if (this.index == 'cityInputAutocompleteShipping') {
                this.exportValue(value, $('[name="novaposhtashipping_city_hidden"]'));
            }
        },
        exportValue: function (value, control) {
            control.val(value);
        },
        exportLabel: function (value, control) {
            control.text(value);
            this.exportValue(value, control);
        },
    });
});
