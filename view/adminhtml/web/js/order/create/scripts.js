/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/* global AdminOrder */
define([
    'jquery',
    'Magento_Sales/order/create/scripts'
], function (jQuery) {
    'use strict';


    AdminOrder.prototype.setShippingMethod = function (method) {
        var data = {};
        var inititalArr = ['shipping_method', 'totals', 'billing_method'];
        if (method === "novaposhtashipping_novaposhtashipping") {
            inititalArr.push('perspective_novaposhta')
        }
        data['order[shipping_method]'] = method;
        this.loadArea(inititalArr, true, data);
    }
});