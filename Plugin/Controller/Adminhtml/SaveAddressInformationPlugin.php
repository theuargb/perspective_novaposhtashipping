<?php

namespace Perspective\NovaposhtaShipping\Plugin\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Quote\Api\Data\CartExtensionFactory;
use Magento\Quote\Model\QuoteAddressValidator;
use Magento\Quote\Model\ShippingAssignmentFactory;
use Magento\Quote\Model\ShippingFactory;
use Perspective\NovaposhtaShipping\Api\Data\ShippingWarehouseInterface;
use Perspective\NovaposhtaShipping\Model\ResourceModel\ShippingWarehouse;
use Psr\Log\LoggerInterface as Logger;

class SaveAddressInformationPlugin extends \Magento\Sales\Controller\Adminhtml\Order\Create\Save
{
    /**
     * @var \Perspective\NovaposhtaShipping\Model\ShippingCheckoutAddressFactory
     */
    private $shippingCheckoutAddressFactory;
    /**
     * @var \Perspective\NovaposhtaShipping\Model\ResourceModel\ShippingWarehouse
     */
    private $shippingCheckoutAddressResourceModel;
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;
    /**
     * @var \Perspective\NovaposhtaShipping\Model\ShippingWarehouse
     */
    protected $checkoutNovaposhtaModel;
    /**
     * @var \Perspective\NovaposhtaCatalog\Model\CityRepository
     */
    private $cityRepository;
    /**
     * @var \Perspective\NovaposhtaShipping\Model\ShippingWarehouse
     */
    public $isDuplicateCheckoutNovaposhtaModel;

    /**
     * SaveAddressInformationPlugin constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Catalog\Helper\Product $productHelper
     * @param \Magento\Framework\Escaper $escaper
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     * @param \Perspective\NovaposhtaShipping\Model\ShippingCheckoutAddressFactory $shippingCheckoutAddressFactory
     * @param ShippingWarehouse $shippingCheckoutAddressResourceModel
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Perspective\NovaposhtaCatalog\Model\CityRepository $cityRepository
     */
    public function __construct(
        Action\Context $context,
        \Magento\Catalog\Helper\Product $productHelper,
        \Magento\Framework\Escaper $escaper,
        PageFactory $resultPageFactory,
        ForwardFactory $resultForwardFactory,
        \Perspective\NovaposhtaShipping\Model\ShippingCheckoutAddressFactory $shippingCheckoutAddressFactory,
        ShippingWarehouse $shippingCheckoutAddressResourceModel,
        \Magento\Framework\App\RequestInterface $request,
        \Perspective\NovaposhtaCatalog\Model\CityRepository $cityRepository
    ) {
        $this->shippingCheckoutAddressFactory = $shippingCheckoutAddressFactory;
        $this->shippingCheckoutAddressResourceModel = $shippingCheckoutAddressResourceModel;
        $this->request = $request;
        $this->cityRepository = $cityRepository;
        parent::__construct($context, $productHelper, $escaper, $resultPageFactory, $resultForwardFactory);
    }

    /**
     * @param \Magento\Sales\Controller\Adminhtml\Order\Create\Save $subject
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function beforeExecute(
        \Magento\Sales\Controller\Adminhtml\Order\Create\Save $subject
    ) {
        /** @var \Perspective\NovaposhtaShipping\Model\ShippingWarehouse $checkoutNovaposhtaModel */
        $this->checkoutNovaposhtaModel = $this->shippingCheckoutAddressFactory->create();
        $this->isDuplicateCheckoutNovaposhtaModel = $this->shippingCheckoutAddressFactory->create();
        $warehouseId = $this->request->getParam('novaposhta_warehouse');
        $cityId = $this->request->getParam('novaposhta_city');
        $cartId = $this->_getQuote()->getId();
        $this->shippingCheckoutAddressResourceModel
            ->load($this->isDuplicateCheckoutNovaposhtaModel, $cartId, ShippingWarehouseInterface::CART_ID);
        if ($this->isDuplicateCheckoutNovaposhtaModel->getId()) {
            $this->checkoutNovaposhtaModel->setId($this->isDuplicateCheckoutNovaposhtaModel->getId());
        }
        $this->checkoutNovaposhtaModel->setCartId(isset($cartId) ? (int)($cartId) : 0);
        $this->checkoutNovaposhtaModel->setCity(!empty($cityId)
            ? (int)($cityId)
            : 0);
        $this->checkoutNovaposhtaModel->setWarehouse(!empty($warehouseId)
            ? (int)($warehouseId)
            : 0);
        $this->checkoutNovaposhtaModel->setCartId(!empty($cartId) ? (int)($cartId) : 0);
        $this->shippingCheckoutAddressResourceModel->save($this->checkoutNovaposhtaModel);
    }
}
