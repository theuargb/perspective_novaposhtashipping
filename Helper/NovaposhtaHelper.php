<?php

namespace Perspective\NovaposhtaShipping\Helper;

use Magento\Framework\DataObject;
use Perspective\NovaposhtaShipping\Model\Carrier\Sender;

class NovaposhtaHelper
{
    const DEFAULT_WEIGHT = 0.1;

    const DEFAULT_HEIGHT = 170;

    const C2C_DELIVERY = 'c2c';

    const C2W_DELIVERY = 'c2w';

    const W2W_DELIVERY = 'w2w';

    const W2C_DELIVERY = 'w2c';

    const PALLETE_THRESHOLD = 85;

    const PALLETE_LENGTH_120 = 120;

    const PALLETE_LENGTH_60 = 60;

    const PALLETE_WIDTH_80 = 80;

    const PALLETE_WEIGHT_204 = 500;

    const PALLETE_WEIGHT_408 = 500;

    /**
     * @var \Perspective\NovaposhtaShipping\Helper\Config
     */
    private $config;

    /**
     * @var \Perspective\NovaposhtaCatalog\Api\CityRepositoryInterface
     */
    private $cityRepository;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterfaceFactory
     */
    private $productRepositoryInterfaceFactory;


    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    private $timezone;

    /**
     * @var \Perspective\NovaposhtaShipping\Helper\BoxpackerFactory
     */
    private $boxpackerFactory;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    private $cartRepository;

    /** @var ServiceType */
    private $serviceType;

    /** @var DeliveryDate */
    private $deliveryDate;

    /** @var ShippingSales */
    private $shippingSales;

    /** @var NovaposhtaApi */
    private $novaposhtaApi;

    /** @var Sender */
    private $sender;

    /**
     * @var \Perspective\NovaposhtaShipping\Model\Carrier\Method\AbstractChain
     */
    private $shippingCartProcessor;

    /**
     * NovaposhtaHelper constructor.
     * @param \Perspective\NovaposhtaShipping\Helper\Config $config
     * @param \Perspective\NovaposhtaCatalog\Api\CityRepositoryInterface $cityRepository
     * @param \Perspective\NovaposhtaShipping\Api\NovaPoshtaApi2Factory $novaPoshtaApi2Factory
     * @param \Perspective\NovaposhtaShipping\Model\CounterpartyAddressIndexFactory $counterpartyAddressIndexFactory
     * @param \Perspective\NovaposhtaShipping\Model\ResourceModel\CounterpartyAddressIndex\CollectionFactory $counterpartyAddressIndexCollectionFactory
     * @param \Magento\Catalog\Api\ProductRepositoryInterfaceFactory $productRepositoryInterfaceFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Perspective\NovaposhtaShipping\Helper\BoxpackerFactory $boxpackerFactory
     * @param \Magento\Quote\Api\CartRepositoryInterface $cartRepository
     * @param \Perspective\NovaposhtaShipping\Model\Carrier\NovaposhtaApi $novaposhtaApi
     * @param \Perspective\NovaposhtaShipping\Model\Carrier\DeliveryDate $deliveryDate
     * @param \Perspective\NovaposhtaShipping\Model\Carrier\ServiceType $serviceType
     * @param \Perspective\NovaposhtaShipping\Model\Carrier\ShippingSales $shippingSales
     * @param \Perspective\NovaposhtaShipping\Model\Carrier\Sender $sender
     * @param \Perspective\NovaposhtaShipping\Model\Carrier\Method\AbstractChain $shippingCartProcessor
     */
    public function __construct(
        \Perspective\NovaposhtaShipping\Helper\Config $config,
        \Perspective\NovaposhtaCatalog\Api\CityRepositoryInterface $cityRepository,
        \Perspective\NovaposhtaShipping\Api\NovaPoshtaApi2Factory $novaPoshtaApi2Factory,
        \Perspective\NovaposhtaShipping\Model\CounterpartyAddressIndexFactory $counterpartyAddressIndexFactory,
        \Perspective\NovaposhtaShipping\Model\ResourceModel\CounterpartyAddressIndex\CollectionFactory $counterpartyAddressIndexCollectionFactory,
        \Magento\Catalog\Api\ProductRepositoryInterfaceFactory $productRepositoryInterfaceFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Perspective\NovaposhtaShipping\Helper\BoxpackerFactory $boxpackerFactory,
        \Magento\Quote\Api\CartRepositoryInterface $cartRepository,
        \Perspective\NovaposhtaShipping\Model\Carrier\NovaposhtaApi $novaposhtaApi,
        \Perspective\NovaposhtaShipping\Model\Carrier\DeliveryDate $deliveryDate,
        \Perspective\NovaposhtaShipping\Model\Carrier\ServiceType $serviceType,
        \Perspective\NovaposhtaShipping\Model\Carrier\ShippingSales $shippingSales,
        \Perspective\NovaposhtaShipping\Model\Carrier\Sender $sender,
        \Perspective\NovaposhtaShipping\Model\Carrier\Method\AbstractChain $shippingCartProcessor

    ) {
        $this->config = $config;
        $this->cityRepository = $cityRepository;
        $this->novaPoshtaApi2Factory = $novaPoshtaApi2Factory;
        $this->counterpartyAddressIndexFactory = $counterpartyAddressIndexFactory;
        $this->counterpartyAddressIndexCollectionFactory = $counterpartyAddressIndexCollectionFactory;
        $this->productRepositoryInterfaceFactory = $productRepositoryInterfaceFactory;
        $this->timezone = $timezone;
        $this->boxpackerFactory = $boxpackerFactory;
        $this->cartRepository = $cartRepository;
        $this->serviceType = $serviceType;
        $this->deliveryDate = $deliveryDate;
        $this->shippingSales = $shippingSales;
        $this->novaposhtaApi = $novaposhtaApi;
        $this->sender = $sender;
        $this->shippingCartProcessor = $shippingCartProcessor;
    }

    public function getShippingPriceByData(array $data)
    {

        $this->prepareSenderCity();
        //Конвертация в объект
        $object = new DataObject($data);
        $quote = $this->cartRepository->get($object->getQuoteId());

        $destinationCityRef = $object->getCurrentUserAddress()->getCity();
        $serviceType = $this->getServiceType($object);

        $items = $quote->getAllItems();
        $totals = $quote->getTotals(); //Total object
        $subtotal = round($totals["subtotal"]->getValue()); //Subtotal value
        $weight = 0;
        foreach ($items as $item) {
            $weight += ($item->getWeight() * $item->getQty());
            $result = $this->markProductWithSpecialPrice($item);
        }
        $optionsSeat = $this->boxpackerFactory->create()->calcSeats($items);
        if (floatval($weight) < self::PALLETE_THRESHOLD) {
            $cargoType = 'Cargo';
        } else {
            $cargoType = 'Pallet';
        }
        $lowerShippingPrice = INF;
        /*
       * Чел-пон вид для поиска нужного района
       */
        $object->setWeight($weight);
        $object->setSubtotal($subtotal);
        $object->setCargoType($cargoType);
        $object->setOptionsSeat($optionsSeat);

        foreach ($this->sender->getSenderCityListArray() as $city) {
            $object->setData('sender_city', $city);
            $price = $this->shippingCartProcessor->process($quote, $object);
            $lowerShippingPriceArr = $this->calculateDeliveryDate($city, $destinationCityRef, $serviceType, $price['data'], $lowerShippingPrice);
        }
        $result = $this->getLowestShippingPriceAmongWarehouses($lowerShippingPriceArr, $result);

        $result = $this->calculateShippingSales($result, $cargoType, $subtotal, $optionsSeat);
        return $result;
    }

    public function getDeliveryDate($sender, $recipient, $type)
    {
        return $this->deliveryDate->getDeliveryDate($sender, $recipient, $type);
    }

    /**
     * @param $code
     * @param $key
     * @param null $storeId
     * @return mixed
     */
    public function getStoreConfigByCode($key, $code, $storeId = null)
    {
        return $this->config->getShippingConfigByCode($key, $code, $storeId);
    }

    /**
     * @return \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    public function getTimezone(): \Magento\Framework\Stdlib\DateTime\TimezoneInterface
    {
        return $this->timezone;
    }

    /**
     * @return \Perspective\NovaposhtaCatalog\Api\CityRepositoryInterface
     */
    public function getCityRepository(): \Perspective\NovaposhtaCatalog\Api\CityRepositoryInterface
    {
        return $this->cityRepository;
    }


    private function prepareSenderCity()
    {
        $this->sender->prepareSenderCity();
    }

    public function getApi()
    {
        return $this->novaposhtaApi->getApi();
    }

    /**
     * @param array $result
     * @param string $cargoType
     * @param float $subtotal
     * @param $optionsSeat
     * @return array
     */
    private function calculateShippingSales(array $result, string $cargoType, float $subtotal, $optionsSeat): array
    {
        return $this->shippingSales->calculateShippingSales($result, $cargoType, $subtotal, $optionsSeat);
    }

    /**
     * @param $city
     * @param $destinationCityRef
     * @param string $service_type
     * @param $data1
     * @param $lowerShippingPrice
     * @param $datum
     * @return array
     */
    private function calculateDeliveryDate($city, $destinationCityRef, string $service_type, $data1, $lowerShippingPrice): array
    {
        return $this->deliveryDate->calculateDeliveryDate($city, $destinationCityRef, $service_type, $data1, $lowerShippingPrice);
    }

    /**
     * @param array $lowerShippingPriceArr
     * @param array $result
     * @return array
     */
    private function getLowestShippingPriceAmongWarehouses(array $lowerShippingPriceArr, array $result): array
    {
        $comparedLowerPrice = INF;
        $comparedLowerDate = $this->timezone->date()->format('d-m-Y');
        if (isset($lowerShippingPriceArr)) {
            foreach ($lowerShippingPriceArr as $index => $value) {
                if ($comparedLowerPrice > $value) {
                    $comparedLowerPrice = $value;
                    $comparedLowerDate = $index;
                }
            }
            $result['price'] = $comparedLowerPrice;
            $result['date'] = $comparedLowerDate;
        }
        return $result;
    }

    /**
     * @param \Magento\Framework\DataObject $object
     * @return string
     */
    private function getServiceType(DataObject $object): string
    {
        return $this->serviceType->getServiceType($object);
    }

    /**
     * @param $item
     * @return array
     */
    private function markProductWithSpecialPrice($item): array
    {
        $productModel = $this->productRepositoryInterfaceFactory->create()->get($item->getProduct()->getSku());
        $specialprice = $productModel->getSpecialPrice();
        $specialPriceFromDate = $productModel->getSpecialFromDate();
        $specialPriceToDate = $productModel->getSpecialToDate();
        $today = time();
        $result = [];
        if ($specialprice && ($productModel->getPrice() > $productModel->getFinalPrice())) {
            if ($today >= strtotime($specialPriceFromDate) && $today <= strtotime($specialPriceToDate) ||
                $today >= strtotime($specialPriceFromDate) && is_null($specialPriceToDate)) {
                $result['sale'] = [$item->getSku()];
            }
        }
        return $result;
    }

}
