<?php

namespace Perspective\NovaposhtaShipping\Helper;

use DVDoug\BoxPacker\PackerFactory;
use DVDoug\BoxPacker\Test\TestBoxFactory;
use DVDoug\BoxPacker\Test\TestItemFactory;
use Magento\Catalog\Api\ProductRepositoryInterfaceFactory;

class Boxpacker
{
    /**
     * @var \DVDoug\BoxPacker\PackerFactory
     */
    private $packerFactory;
    /**
     * @var \DVDoug\BoxPacker\Test\TestBoxFactory
     */
    private $boxFactory;
    /**
     * @var \DVDoug\BoxPacker\Test\TestItemFactory
     */
    private $itemFactory;
    private $packer;
    /**
     * @var array
     */
    private $finalOptionSeats;
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterfaceFactory
     */
    private $productRepositoryInterfaceFactory;
    private $packedBoxes;
    private $overallWeight;

    public function __construct(
        PackerFactory $packerFactory,
        TestBoxFactory $BoxFactory,
        TestItemFactory $ItemFactory,
        ProductRepositoryInterfaceFactory $productRepositoryInterfaceFactory
    ) {
        $this->packerFactory = $packerFactory;
        $this->boxFactory = $BoxFactory;
        $this->itemFactory = $ItemFactory;
        $this->productRepositoryInterfaceFactory = $productRepositoryInterfaceFactory;
    }

    public function calcSeats($products)
    {
        $this->finalOptionSeats = [];
        $this->packer = $this->packerFactory->create();
        foreach ($products as $productIdx => $productVal) {
            $this->overallWeight += ($productVal->getWeight() * $productVal->getQty());
        }
        $this->setAvailableBoxes();
        foreach ($products as $productIdx => $productVal) {
            $productModel = $this->productRepositoryInterfaceFactory->create()->get($productVal->getProduct()->getSku());
            $widthProduct = (float)$productModel->getProductWidth() / 10;
            $lengthProduct = (float)floatval($productModel->getProductLength()) / 10;
            $heightProduct = (float)$productModel->getProductHeight() / 10;
            $weightProduct = (float)$productModel->getWeight() * 1000;
            $this->packer->addBox($this->boxFactory->create([
                'reference' => 'product_size_box',
                'outerWidth' => $widthProduct,
                'outerLength' => $lengthProduct,
                'outerDepth' => $heightProduct,
                'emptyWeight' => 0,
                'innerWidth' => $widthProduct,
                'innerLength' => $lengthProduct,
                'innerDepth' => $heightProduct,
                'maxWeight' => $weightProduct
            ]));
            if ($productVal instanceof \Magento\Sales\Model\Order\Item || $productVal instanceof \Magento\Sales\Model\Order\Item\Interceptor) {
                $this->packer->addItem($this->itemFactory->create([
                    'description' => $productIdx,
                    'width' => $widthProduct,
                    'length' => $lengthProduct,
                    'depth' => $heightProduct,
                    'weight' => $weightProduct,
                    'keepFlat' => false
                ]), $productVal->getQtyToShip());
            } else {
                $this->packer->addItem($this->itemFactory->create([
                    'description' => $productIdx,
                    'width' => $widthProduct,
                    'length' => $lengthProduct,
                    'depth' => $heightProduct,
                    'weight' => $weightProduct,
                    'keepFlat' => false
                ]), $productVal->getQty());
            }
        }
        $this->packedBoxes = $this->packer->pack();
        foreach ($this->packedBoxes as $box) {
            if (($box->getWeight() / 1000) < NovaposhtaHelper::PALLETE_THRESHOLD) {
                if ($box->getUsedLength() / 10 !== 0) {
                    $length = $box->getUsedLength() / 10;
                } else {
                    $length = $lengthProduct;
                }
                if ($box->getUsedDepth() / 10 !== 0) {
                    $height = $box->getUsedDepth() / 10;
                } else {
                    $height = $heightProduct;
                }
                if ($box->getUsedWidth() / 10 !== 0) {
                    $width = $box->getUsedWidth() / 10;
                } else {
                    $width = $widthProduct;
                }
                $this->finalOptionSeats[] = [
                    'volumetricVolume' => "",
                    'volumetricWidth' => $width,
                    'volumetricHeight' => $height,
                    'volumetricLength' => $length,
                    'weight' => $box->getWeight() / 1000,
                ];
            } else {
                $this->finalOptionSeats[] = [
                    'volumetricVolume' => "",
                    'volumetricWidth' =>  $box->getUsedWidth() / 10,
                    'volumetricHeight' => $box->getUsedWidth() / 10,
                    'volumetricLength' => $box->getUsedWidth() / 10,
                    'weight' => $box->getBox()->getMaxWeight() / 1000,
                ];
            }

            //вместо реальных габаритов товаров, используем НПшные, бо "неправильно считает"
//            $this->finalOptionSeats[] = [
//                'volumetricVolume' => "",
//                'volumetricWidth' => $box->getUsedWidth() / 10,
//                'volumetricHeight' => $box->getUsedDepth() / 10,
//                'volumetricLength' => $box->getUsedLength() / 10,
//                'weight' => $box->getWeight() / 1000,
//            ];
        }
        return isset($this->finalOptionSeats) ? $this->finalOptionSeats : [];
    }

    protected function setAvailableBoxes()
    {
        /**
         * все значения в граммах и миллиметрах, кроме
         * @see overallWeight
         * оно в кг
         */
        //от 500 до 750 кг - 612, 408
        if ($this->overallWeight >= 500 && $this->overallWeight <= 750) {
            $this->packer->addBox($this->boxFactory->create(
                [
                    'reference' => '80*120*170',
                    'outerWidth' => 800,
                    'outerLength' => 1200,
                    'outerDepth' => 1700,
                    'emptyWeight' => 0,
                    'innerWidth' => 800,
                    'innerLength' => 1200,
                    'innerDepth' => 1700,
                    'maxWeight' => 500000
                ]
            ));
            $this->packer->addBox($this->boxFactory->create(
                [
                    'reference' => '120*120*170',
                    'outerWidth' => 1200,
                    'outerLength' => 1200,
                    'outerDepth' => 1700,
                    'emptyWeight' => 0,
                    'innerWidth' => 1200,
                    'innerLength' => 1200,
                    'innerDepth' => 1700,
                    'maxWeight' => 750000
                ]
            ));
        }
        //от 0 до 500 кг - 408
        if ($this->overallWeight >= 0 && $this->overallWeight < 500) {
            $this->packer->addBox($this->boxFactory->create(
                [
                    'reference' => '80*120*170',
                    'outerWidth' => 800,
                    'outerLength' => 1200,
                    'outerDepth' => 1700,
                    'emptyWeight' => 0,
                    'innerWidth' => 800,
                    'innerLength' => 1200,
                    'innerDepth' => 1700,
                    'maxWeight' => 500000
                ]
            ));
        }
        //от 750 до 1000+ кг - 816, 612, 408
        if ($this->overallWeight > 750) {
            $this->packer->addBox($this->boxFactory->create(
                [
                    'reference' => '80*120*170',
                    'outerWidth' => 800,
                    'outerLength' => 1200,
                    'outerDepth' => 1700,
                    'emptyWeight' => 0,
                    'innerWidth' => 800,
                    'innerLength' => 1200,
                    'innerDepth' => 1700,
                    'maxWeight' => 500000
                ]
            ));
            $this->packer->addBox($this->boxFactory->create(
                [
                    'reference' => '120*120*170',
                    'outerWidth' => 1200,
                    'outerLength' => 1200,
                    'outerDepth' => 1700,
                    'emptyWeight' => 0,
                    'innerWidth' => 1200,
                    'innerLength' => 1200,
                    'innerDepth' => 1700,
                    'maxWeight' => 750000
                ]
            ));
            $this->packer->addBox($this->boxFactory->create(
                [
                    'reference' => '141*141*170',
                    'outerWidth' => 1410,
                    'outerLength' => 1410,
                    'outerDepth' => 1700,
                    'emptyWeight' => 0,
                    'innerWidth' => 1410,
                    'innerLength' => 1410,
                    'innerDepth' => 1700,
                    'maxWeight' => 1000000
                ]
            ));
        }
        if (!isset($this->overallWeight)) {
            //все доступные палеты НП, на случай если товар битый и невозможно получить вес
            // $this->packer->addBox(new TestBox('80*60*170', 800, 600, 1700, 0, 800, 600, 1700, 500000));
            $this->packer->addBox($this->boxFactory->create(
                [
                    'reference' => '80*120*170',
                    'outerWidth' => 800,
                    'outerLength' => 1200,
                    'outerDepth' => 1700,
                    'emptyWeight' => 0,
                    'innerWidth' => 800,
                    'innerLength' => 1200,
                    'innerDepth' => 1700,
                    'maxWeight' => 500000
                ]
            ));
            $this->packer->addBox($this->boxFactory->create(
                [
                    'reference' => '120*120*170',
                    'outerWidth' => 1200,
                    'outerLength' => 1200,
                    'outerDepth' => 1700,
                    'emptyWeight' => 0,
                    'innerWidth' => 1200,
                    'innerLength' => 1200,
                    'innerDepth' => 1700,
                    'maxWeight' => 750000
                ]
            ));
            $this->packer->addBox($this->boxFactory->create(
                [
                    'reference' => '141*141*170',
                    'outerWidth' => 1410,
                    'outerLength' => 1410,
                    'outerDepth' => 1700,
                    'emptyWeight' => 0,
                    'innerWidth' => 1410,
                    'innerLength' => 1410,
                    'innerDepth' => 1700,
                    'maxWeight' => 1000000
                ]
            ));
        }
    }
}
