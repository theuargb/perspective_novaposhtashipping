<?php

namespace Perspective\NovaposhtaShipping\Model;

use Perspective\NovaposhtaShipping\Model\ResourceModel\ShippingAddress as ResourceModel;
use Perspective\NovaposhtaShipping\Model\ShippingAddressFactory as ModelFactory;
use Perspective\NovaposhtaShipping\Model\ShippingAddress as Model;

class ShippingAddressRepository implements \Perspective\NovaposhtaShipping\Api\ShippingAddressRepositoryInterface
{
    /**
     * @var ModelFactory
     */
    private $modelFactory;

    /**
     * @var ResourceModel
     */
    private $resourceModel;

    public function __construct(
        ModelFactory $modelFactory,
        ResourceModel $resourceModel
    ) {
        $this->modelFactory = $modelFactory;
        $this->resourceModel = $resourceModel;
    }

    public function loadAddressInfo($quoteId)
    {
        /** @var Model $model */
        $model = $this->modelFactory->create();
        $this->resourceModel->load($model, $quoteId, 'cart_id');
        if (!$model->getId()) {
            //задаем дефолтные значения
            $model->setCity('e221d64c-391c-11dd-90d9-001a92567626');
            $model->setStreet('29207dc8-7aa5-11df-be73-000c290fbeaa');
            $model->setBuilding('1');
            $model->setFlat('1');
        }
        return $model;
    }
}
