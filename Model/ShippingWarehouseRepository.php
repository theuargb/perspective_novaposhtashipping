<?php

namespace Perspective\NovaposhtaShipping\Model;

use Perspective\NovaposhtaShipping\Model\ResourceModel\ShippingWarehouse as ResourceModel;
use Perspective\NovaposhtaShipping\Model\ShippingWarehouseFactory as ModelFactory;
use Perspective\NovaposhtaShipping\Model\ShippingWarehouse as Model;

class ShippingWarehouseRepository implements \Perspective\NovaposhtaShipping\Api\ShippingWarehouseRepositoryInterface
{
    /**
     * @var ModelFactory
     */
    private $modelFactory;

    /**
     * @var ResourceModel
     */
    private $resourceModel;

    public function __construct(
        ModelFactory $modelFactory,
        ResourceModel $resourceModel
    ) {
        $this->modelFactory = $modelFactory;
        $this->resourceModel = $resourceModel;
    }

    public function loadAddressInfo($quoteId)
    {
        /** @var Model $model */
        $model = $this->modelFactory->create();
        $this->resourceModel->load($model, $quoteId, 'cart_id');
        if (!$model->getId()) {
            //задаем дефолтные значения
            $model->setCity('e221d64c-391c-11dd-90d9-001a92567626');
            $model->setWarehouse('e221d64c-391c-11dd-90d9-001a92567626');
        }
        return $model;
    }
}
