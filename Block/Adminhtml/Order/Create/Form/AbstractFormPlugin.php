<?php

namespace Perspective\NovaposhtaShipping\Block\Adminhtml\Order\Create\Form;

use Magento\Framework\Data\Form;
use Magento\Framework\Data\Form\Element\Hidden;
use Magento\Framework\Data\Form\Element\Select;
use Magento\Framework\Data\Form\Element\Text;
use Magento\Sales\Block\Adminhtml\Order\Create\Form\AbstractForm;
use Perspective\NovaposhtaShipping\Block\Adminhtml\Controls\Select2;

/**
 * Не получается добавить вид name как у остальных полей
 */
class AbstractFormPlugin
{
    const NOVAPOSHTA_SHIPPING_CITY_VALUE = 'novaposhtashipping_city';

    const NOVAPOSHTA_SHIPPING_CITY_HIDDEN_VALUE = 'novaposhtashipping_city_hidden';

    /**
     * @param AbstractForm $subject
     * @param Form $result
     * @return Form
     */
    public function afterGetForm(AbstractForm $subject, Form $result): Form
    {
        if (!$result->getElement(self::NOVAPOSHTA_SHIPPING_CITY_HIDDEN_VALUE)) {
            $result = $this->createCityHiddenField($result);
        }
        if (!$result->getElement(self::NOVAPOSHTA_SHIPPING_CITY_VALUE)) {
            $result = $this->createCitySelect($result);
        }
        return $result;
    }

    /**
     * @param \Magento\Framework\Data\Form $result
     * @return \Magento\Framework\Data\Form\Element\AbstractElement|null
     */
    private function createCitySelect(Form $result): ?\Magento\Framework\Data\Form
    {
        $result->getElement('main')->addField(self::NOVAPOSHTA_SHIPPING_CITY_VALUE, Select2::class, [], self::NOVAPOSHTA_SHIPPING_CITY_HIDDEN_VALUE);
        $element = $result->getElement(self::NOVAPOSHTA_SHIPPING_CITY_VALUE);
        $element->setData('label', __('City'));
        $element->setData('name', self::NOVAPOSHTA_SHIPPING_CITY_VALUE);
        $element->setData('required', true);
        $prefix = explode('-', $result->getHtmlIdPrefix());
        if ($prefix && is_array($prefix) && count($prefix) > 1) {
            if ($prefix[1] == 'billing_address_') {
                $dataBindArray['scope'] = '\'cityInputAutocompleteBilling\'';
            }
            if ($prefix[1] == 'shipping_address_') {
                $dataBindArray['scope'] = '\'cityInputAutocompleteShipping\'';
            }
        }
        $element->setDataBind($dataBindArray);

        return $result;
    }

    /**
     * @param \Magento\Framework\Data\Form $result
     */
    private function createCityHiddenField(Form $result): ?\Magento\Framework\Data\Form
    {
        $result->getElement('main')->addField(self::NOVAPOSHTA_SHIPPING_CITY_HIDDEN_VALUE, Hidden::class, [], 'city');
        $element = $result->getElement(self::NOVAPOSHTA_SHIPPING_CITY_HIDDEN_VALUE);
        $element->setData('label', __('City hidden'));
        $element->setData('name', self::NOVAPOSHTA_SHIPPING_CITY_HIDDEN_VALUE);
        return $result;
    }
}
