<?php

namespace Perspective\NovaposhtaShipping\Block\Adminhtml\Order\Shipping;

use Exception;
use Magento\Backend\Block\Template\Context;
use Magento\Catalog\Api\ProductRepositoryInterfaceFactory;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Block\Adminhtml\Order\AbstractOrder;
use Magento\Sales\Helper\Admin;
use Magento\Shipping\Helper\Data as ShippingHelper;
use Magento\Tax\Helper\Data as TaxHelper;
use Perspective\NovaposhtaCatalog\Api\CityRepositoryInterface;
use Perspective\NovaposhtaShipping\Helper\BoxpackerFactory;
use Perspective\NovaposhtaShipping\Helper\Config;
use Perspective\NovaposhtaShipping\Helper\NovaposhtaHelper;
use Perspective\NovaposhtaShipping\Model\ResourceModel\CounterpartyAddressIndex\CollectionFactory;
use Perspective\NovaposhtaShipping\Model\ResourceModel\CounterpartyIndex\CollectionFactory as CounterpartyIndexCollectionFactoryAlias;
use Perspective\NovaposhtaShipping\Model\ResourceModel\CounterpartyOrgThirdparty\CollectionFactory as CounterpartyOrgThirdpartyCollectionFactory;
use Perspective\NovaposhtaShipping\Model\ResourceModel\ShippingAddress\Collection;
use Psr\Log\LoggerInterface;

/**
 * Class Npc2cshipment
 * @package Perspective\NovaposhtaShipping\Block\Adminhtml\Order\Shipping
 */
class Npc2cshipment extends AbstractOrder
{
    /**
     * @var \Magento\Backend\Block\Template\Context
     */
    private $context;
    /**
     * @var \Magento\Framework\Registry
     */
    private $registry;
    /**
     * @var \Magento\Sales\Helper\Admin
     */
    private $adminHelper;
    /**
     * @var \Perspective\NovaposhtaShipping\Model\ResourceModel\ShippingWarehouse\Collection
     */
    private $shippingCheckoutAddressResourceModelCollection;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;
    /**
     * @var array
     */
    private $data;
    /**
     * @var \Magento\Shipping\Helper\Data|null
     */
    private $shippingHelper;
    /**
     * @var \Magento\Tax\Helper\Data|null
     */
    private $taxHelper;
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterfaceFactory
     */
    private $productRepositoryInterfaceFactory;
    /**
     * @var \Perspective\NovaposhtaShipping\Helper\BoxpackerFactory
     */
    private $boxpackerFactory;
    /**
     * @var \Perspective\NovaposhtaShipping\Helper\NovaposhtaHelper
     */
    private $novaposhtaHelper;
    /**
     * @var \Perspective\NovaposhtaCatalog\Api\CityRepositoryInterface
     */
    private $cityRepository;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $dateTime;
    /**
     * @var \Perspective\NovaposhtaShipping\Helper\Config
     */
    private $config;
    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $orderRepository;
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;
    /**
     * @var \Perspective\NovaposhtaShipping\Model\ResourceModel\CounterpartyAddressIndex\CollectionFactory
     */
    private $counterpartyAddressIndexCollectionFactory;
    /**
     * @var \Perspective\NovaposhtaShipping\Model\ResourceModel\CounterpartyIndex\CollectionFactory
     */
    private $counterpartyIndexCollectionFactory;
    /**
     * @var \Perspective\NovaposhtaShipping\Model\ResourceModel\CounterpartyOrgThirdparty\CollectionFactory
     */
    private $counterpartyOrgThirdpartyCollectionFactory;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    private $timezone;

    /**
     * Npc2cshipment constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Sales\Helper\Admin $adminHelper
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Catalog\Api\ProductRepositoryInterfaceFactory $productRepositoryInterfaceFactory
     * @param \Perspective\NovaposhtaShipping\Helper\BoxpackerFactory $boxpackerFactory
     * @param \Perspective\NovaposhtaShipping\Helper\NovaposhtaHelper $novaposhtaHelper
     * @param \Perspective\NovaposhtaShipping\Model\ResourceModel\ShippingAddress\Collection $shippingCheckoutAddressResourceModelCollection
     * @param \Perspective\NovaposhtaCatalog\Api\CityRepositoryInterface $cityRepository
     * @param \Perspective\NovaposhtaShipping\Helper\Config $config
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Perspective\NovaposhtaShipping\Model\ResourceModel\CounterpartyAddressIndex\CollectionFactory $counterpartyAddressIndexCollectionFactory
     * @param \Perspective\NovaposhtaShipping\Model\ResourceModel\CounterpartyIndex\CollectionFactory $counterpartyIndexCollectionFactory
     * @param \Perspective\NovaposhtaShipping\Model\ResourceModel\CounterpartyOrgThirdparty\CollectionFactory $counterpartyOrgThirdpartyCollectionFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param array $data
     * @param \Magento\Shipping\Helper\Data|null $shippingHelper
     * @param \Magento\Tax\Helper\Data|null $taxHelper
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Admin $adminHelper,
        LoggerInterface $logger,
        ProductRepositoryInterfaceFactory $productRepositoryInterfaceFactory,
        BoxpackerFactory $boxpackerFactory,
        NovaposhtaHelper $novaposhtaHelper,
        Collection $shippingCheckoutAddressResourceModelCollection,
        CityRepositoryInterface $cityRepository,
        Config $config,
        DateTime $dateTime,
        OrderRepositoryInterface $orderRepository,
        CollectionFactory $counterpartyAddressIndexCollectionFactory,
        CounterpartyIndexCollectionFactoryAlias $counterpartyIndexCollectionFactory,
        CounterpartyOrgThirdpartyCollectionFactory $counterpartyOrgThirdpartyCollectionFactory,
        TimezoneInterface $timezone,
        ProductMetadataInterface $productMetadata,
        array $data = [],
        ShippingHelper $shippingHelper = null,
        TaxHelper $taxHelper = null
    ) {
        $this->context = $context;
        $this->request = $context->getRequest();
        $this->shippingCheckoutAddressResourceModelCollection = $shippingCheckoutAddressResourceModelCollection;
        $this->logger = $logger;
        $this->productRepositoryInterfaceFactory = $productRepositoryInterfaceFactory;
        $this->boxpackerFactory = $boxpackerFactory;
        $this->novaposhtaHelper = $novaposhtaHelper;
        $this->cityRepository = $cityRepository;
        $this->dateTime = $dateTime;
        $this->config = $config;
        $this->orderRepository = $orderRepository;
        $this->counterpartyAddressIndexCollectionFactory = $counterpartyAddressIndexCollectionFactory;
        $this->counterpartyIndexCollectionFactory = $counterpartyIndexCollectionFactory;
        $this->counterpartyOrgThirdpartyCollectionFactory = $counterpartyOrgThirdpartyCollectionFactory;
        $this->timezone = $timezone;
        try {
            $this->npAddressData = $this->getQuoteAddressClient();
        } catch (Exception $exception) {
            $this->logger->critical($exception->getTraceAsString());
            $this->logger->debug($exception->getTraceAsString());
        }
        // если текущая версии выше 2.3 то выполняем это
        if (version_compare($productMetadata->getVersion(), '2.3') === 1) {
            parent::__construct($context, $registry, $adminHelper, $data, $shippingHelper, $taxHelper);
        } else {
            //иначе это
            parent::__construct(
                $context,
                $registry,
                $adminHelper,
                $data
            );
        }
    }

    /**
     * @var string
     */
    protected $code = 'c2c';
    /**
     * @var null
     */
    protected $npAddressData = null;
    /**
     * @var array|mixed|null
     */
    private $_senderCityObjArr;
    /**
     * @var array|mixed|null
     */
    private $destinationCityRef;
    /**
     * @var array|mixed|null
     */
    private $deliveryDate;
    /**
     * @var array|mixed|null
     */
    private $deiveryPrice;
    /**
     * @var array|mixed|null
     */
    private $allThirdpartyCounterparties;
    /**
     * @var array|mixed|null
     */
    private $counterpartyAddress;

    /**
     * @var array|mixed|null
     */
    private $optionsSeat;

    /**
     * @return mixed
     */
    public function getCounterPartyToSend()
    {
        $allThirdpartyCounterparties = $this->counterpartyOrgThirdpartyCollectionFactory->create()
            ->getItems();
        foreach ($allThirdpartyCounterparties as $idx => $value) {
            $counterpartyCityRef = $value->getCityRef();
            $counterpartyRef = $value->getRef();
            $allThirdpartyCounterpartiesArr[$counterpartyRef] = $this->cityRepository->getCityByCityRef($counterpartyCityRef)->getDescriptionUa();
        }
        $this->allThirdpartyCounterparties = $allThirdpartyCounterparties;
        return $allThirdpartyCounterpartiesArr;
    }

    /**
     * @return array|mixed|null
     */
    public function getCounterPartyAddressToSend()
    {
        $counterpartyAddress = [];
        $parentCounterparties = $this->counterpartyIndexCollectionFactory->create()->getItems();
        /** @var \Perspective\NovaposhtaShipping\Model\CounterpartyIndex $value */
        foreach ($parentCounterparties as $index => $value) {
            /** @var \Perspective\NovaposhtaShipping\Model\ResourceModel\CounterpartyOrgThirdparty\Collection $counterparty */
            $counterparty = $this->counterpartyOrgThirdpartyCollectionFactory->create()
                ->addFieldToSelect('*')
                ->addFieldToFilter('counterpartyRef', ['like' => $value->getCounterpartyRef()]);
            /** @var \Perspective\NovaposhtaShipping\Model\CounterpartyOrgThirdparty $valueCounterP */
            foreach ($counterparty as $indexCounterP => $valueCounterP) {
                $this->counterpartyAddress[$indexCounterP]['counterpartyRef'] = $valueCounterP->getCounterpartyRef();
                $this->counterpartyAddress[$indexCounterP]['Ref'] = $valueCounterP->getRef();
//                $counterpartyAddress[$indexCounterP]['Addresses'] = $valueCounterP->getAddresses();
                $addresses = $valueCounterP->getAddresses();
                if (isset($addresses['DoorsAddresses'])) {
                    foreach ($addresses['DoorsAddresses'] as $addressIndex => $addressValue) {
                        $this->counterpartyAddress[$indexCounterP]['Addresses'][$addressValue['Ref']] =
                            $addressValue['Type']
                            . ' ' .
                            $addressValue['SettlementDescription']
                            . ' ' .
                            $addressValue['StreetsType']
                            . ' ' .
                            $addressValue['StreetDescription']
                            . ' ' .
                            $addressValue['BuildingNumber']
                            . '. ';
                    }
                }
                $this->counterpartyAddress[$indexCounterP]['Description'] = $valueCounterP->getDescription();
            }
        }

        return $this->counterpartyAddress;
    }

    /**
     * @return mixed
     * @deprecated Не годится из-за того, что в дальнейшем понадобистся Реф контрагента. Все данные будут идти с индекса контрагентов
     * @see getCitiesDataForSelect
     */
    public function getCitiesSenderForSelect()
    {
        $senderCityArr = explode(',', $this->novaposhtaHelper->getStoreConfigByCode('novaposhtashipping', 'sender_city'));
        foreach ($senderCityArr as $idx => $value) {
            $senderCityObjArr[$idx] = $this->cityRepository->getCityByCityRef($value);
        }
        return $senderCityObjArr;
    }

    /**
     * @return mixed
     */
    public function getCitiesDataForSelect()
    {
        $counterpartyAddressIndexCollection = $this->counterpartyAddressIndexCollectionFactory->create()->getItems();
        /** @var \Perspective\NovaposhtaShipping\Model\CounterpartyAddressIndex $value */
        $alrearyPushed = [];
        foreach ($counterpartyAddressIndexCollection as $idx => $value) {
            if (($value->getCityDescription() && $value->getCityRef() && $value->getCounterpartyRef()) && !in_array($value->getCityRef(), $alrearyPushed)) {
                $senderObjArr[$idx] = [
                    'CityDescription' => $value->getCityDescription(),
                    'CityRef' => $value->getCityRef(),
                    'CounterpartyRef' => $value->getCounterpartyRef(),
                ];
                $alrearyPushed[] = $value->getCityRef();
            }
        }
        return $senderObjArr;
    }

    /**
     * @return void
     */
    protected function getCitiesSender()
    {
        $senderCityArr = explode(',', $this->novaposhtaHelper->getStoreConfigByCode('novaposhtashipping', 'sender_city'));
        foreach ($senderCityArr as $idx => $value) {
            $this->_senderCityObjArr[$idx] = $this->cityRepository->getCityByCityId($value);
        }
        foreach ($this->_senderCityObjArr as $value) {
            $result[] = $value->getId();
        }

        if (isset($this->_senderCityObjArr)) {
            $this->_senderCityObjArr;
        } else {
            //ситиИд 4 - Киев
            $this->_senderCityObjArr[] = $this->cityRepository->getCityByCityId(4);
        }
    }

    /**
     * ничего не возвращает, потому-что ранее были заюзаны
     * @throws \Zend_Date_Exception
     * @see setDeiveryPrice
     *
     * @see setDeliveryDate
     * и
     */
    public function getRecalculatedPrice()
    {
        $items = $this->getOrder()->getAllItems();
        $subtotal = round($this->getOrder()->getSubtotalInclTax());
        $weight = 0;
        $this->optionsSeat = [];
        foreach ($items as $item) {
            $weight += ($item->getWeight() * $item->getQtyToShip());
            $productModel = $this->productRepositoryInterfaceFactory->create()->get($item->getProduct()->getSku());
            $specialprice = $productModel->getSpecialPrice();
            $specialPriceFromDate = $productModel->getSpecialFromDate();
            $specialPriceToDate = $productModel->getSpecialToDate();
            $today = time();
            if ($specialprice && ($productModel->getPrice() > $productModel->getFinalPrice())) {
                if ($today >= strtotime($specialPriceFromDate) && $today <= strtotime($specialPriceToDate) ||
                    $today >= strtotime($specialPriceFromDate) && is_null($specialPriceToDate)) {
                    $result['sale'] = [$item->getSku()];
                }
            }
        }
        $this->optionsSeat = $this->boxpackerFactory->create()->calcSeats($items);
        if ($weight < $this->novaposhtaHelper::PALLETE_THRESHOLD) {
            $cargoType = 'Cargo';
        } else {
            $cargoType = 'Pallet';
        }
        //which warehouse closer to customer
        $lowerShippingPrice = INF;
        if (!$this->destinationCityRef) {
            if ($this->getCityData()) {
                if ($this->cityRepository->getCityByCityId($this->getCityData())->getId()) {
                    $this->destinationCityRef = $this->cityRepository->getCityByCityId($this->getCityData())->getRef();
                } else {
                    $this->destinationCityRef = '8d5a980d-391c-11dd-90d9-001a92567626';
                }
            } else {
                $this->destinationCityRef = '8d5a980d-391c-11dd-90d9-001a92567626';
            }
        }
        $saleSender = $this->novaposhtaHelper->getStoreConfigByCode('novaposhtashipping', 'sale_sender');
        $defaultSaleSenderCounterparty = '';
        if ($saleSender) {
            $saleSenderData = explode(',', $saleSender);
            $defaultSaleSenderCounterparty = $saleSenderData[0];
            $defaultSaleCitySenderCounterparty = $saleSenderData[1];
        }
        $defaultSaleContactSenderData = '';
        $saleContactSender = $this->novaposhtaHelper->getStoreConfigByCode('novaposhtashipping', 'sale_sender_contact');
        if ($saleContactSender) {
            $defaultSaleContactSenderData = $saleContactSender;
        }
        $saleContactAddressSender = $this->novaposhtaHelper->getStoreConfigByCode('novaposhtashipping', 'sale_sender_contact_address');
        $defaultSaleContactSenderAddressData = '';
        if ($saleContactAddressSender) {
            $defaultSaleContactSenderAddressData = $saleContactAddressSender;
        }
        /*
         * Чел-пон вид для поиска нужного района
         */
        $cityRecipientString = $this->cityRepository->getCityByCityId($this->getCityData())->getDescriptionUa();
        $Firstname = $this->getOrder()->getShippingAddress()->getFirstname();
        $LastName = $this->getOrder()->getShippingAddress()->getLastname();
        $MiddleName = $this->getOrder()->getShippingAddress()->getMiddlename() ? $this->getOrder()->getShippingAddress()->getMiddlename() : 'Николаевич';
        $Phone = $recipientPhone = $this->getOrder()->getShippingAddress()->getTelephone();
        $streetRecipient = $this->getStreetData();
        $buildingRecipient = $this->getBuildNumData() ? $this->getBuildNumData() : '1';
        $flatRecipient = $this->getFlat() ? $this->getFlat() : '1';
        $recipientType = 'PrivatePerson';
        $paymentMethod = $this->getOrder()->getPayment()->getMethod() == 'cashondelivery' ? "Cash" : "NonCash";
        $AreaAndRegionData = $this->novaposhtaHelper->getApi()->request(
            'Address',
            'searchSettlements',
            [
                'calledMethod' => 'searchSettlements',
                'CityName' => $cityRecipientString,
                'Limit' => 100,
            ]
        );
        $areaRecipient = '';
        $regionRecipient = '';
        if (array_key_exists('success', $AreaAndRegionData)) {
            foreach ($AreaAndRegionData['data'][0]['Addresses'] as $inx => $datum) {
                if ($datum['DeliveryCity'] === $this->cityRepository->getCityByCityId($this->getCityData())->getRef()) {
                    $areaRecipient = $datum['Area'];
                    $regionRecipient = $datum['Region'];
                }
            }
        }
        $this->getCitiesSender();
        foreach ($this->_senderCityObjArr as $cityIdx => $city) {
            if ($this->getOrder()->getPayment()->getMethod() === 'cashondelivery') {
                $price = $this->novaposhtaHelper->getApi()->request('InternetDocument', 'getDocumentPrice', [
                    'CitySender' => $city->getRef(),
                    'CityRecipient' => $this->destinationCityRef,
                    'NewAddress' => 1,
                    'Sender' => $defaultSaleSenderCounterparty,
                    'PayerType' => 'Sender',
                    'ContactSender' => $defaultSaleContactSenderData,
//                    'SenderAddress' => $defaultSaleContactSenderAddressData,
                    'SenderAddress' => $defaultSaleContactSenderAddressData,
                    'PaymentMethod' => $paymentMethod,
                    'Description' => 'плитка (керамічна, гранітна, мозаїка)',
                    'RecipientCityName' => $cityRecipientString,
                    /*
                    * следующие поля требуют дополнительной работы с импортом городов. нужно по имени населенного пункта найти область, район и т.д.
                    */
                    'RecipientArea' => $areaRecipient,
                    'RecipientAreaRegions' => $regionRecipient,
                    /* ^
                     * |
                     * эти поля
                     */
                    'RecipientAddressName' => $streetRecipient,
                    'RecipientHouse' => $buildingRecipient,
                    'RecipientFlat' => $flatRecipient,
                    'RecipientName' => $Firstname . ' ' . $MiddleName . ' ' . $LastName,
                    'RecipientType' => $recipientType, //PrivatePerson для всех
                    'RecipientsPhone' => $Phone,
                    'DateTime' => $this->dateTime->date('d.m.Y'),

                    'ServiceType' => 'DoorsDoors',
                    'Weight' => $weight,
                    'Cost' => round($this->getOrder()->getSubtotalInclTax()),
                    'CargoType' => $cargoType,
                    'OptionsSeat' => $this->optionsSeat,
                    'VolumeGeneral' => null,
                    'RedeliveryCalculate' => [
                        'CargoType' => 'Money',
                        'Amount' => round($this->getOrder()->getSubtotalInclTax())
                    ]
                ]);
            } else {
                $price = $this->novaposhtaHelper->getApi()->request('InternetDocument', 'getDocumentPrice', [
                    'CitySender' => $city->getRef(),
                    'CityRecipient' => $this->destinationCityRef,
                    'NewAddress' => 1,
                    'Sender' => $defaultSaleSenderCounterparty,
                    'PayerType' => 'Sender',
                    'ContactSender' => $defaultSaleContactSenderData,
//                    'SenderAddress' => $defaultSaleContactSenderAddressData,
                    'SenderAddress' => $defaultSaleContactSenderAddressData,
                    'PaymentMethod' => $paymentMethod,
                    'Description' => 'плитка (керамічна, гранітна, мозаїка)',
                    'RecipientCityName' => $cityRecipientString,
                    /*
                    * следующие поля требуют дополнительной работы с импортом городов. нужно по имени населенного пункта найти область, район и т.д.
                    */
                    'RecipientArea' => $areaRecipient,
                    'RecipientAreaRegions' => $regionRecipient,
                    /* ^
                     * |
                     * эти поля
                     */
                    'RecipientAddressName' => $streetRecipient,
                    'RecipientHouse' => $buildingRecipient,
                    'RecipientFlat' => $flatRecipient,

                    'RecipientName' => $Firstname . ' ' . $MiddleName . ' ' . $LastName,
                    'RecipientType' => $recipientType, //PrivatePerson для всех
                    'RecipientsPhone' => $Phone,
                    'DateTime' => $this->dateTime->date('d.m.Y'),

                    'ServiceType' => 'DoorsDoors',
                    'Weight' => $weight,
                    'Cost' => round($this->getOrder()->getSubtotalInclTax()),
                    'CargoType' => $cargoType,
                    'OptionsSeat' => $this->optionsSeat,
                    'VolumeGeneral' => null,
                ]);
            }
            $deliveryDate = $this->novaposhtaHelper->getDeliveryDate($city->getRef(), $this->destinationCityRef, 'DoorsDoors');
            $deliveryDate['cityRef'] = $city->getRef();
            $price['cityRef'] = $city->getRef();
            $this->setDeliveryDate($deliveryDate);
            $this->setDeiveryPrice($price);
            if (isset($price['data'])) {
                foreach ($price['data'] as $datum) {
                    if ($datum['Cost'] < $lowerShippingPrice) {
                        if ($lowerShippingPrice === INF) {
                            $lowerShippingPrice = 0;
                        }
                        if (isset($datum['CostRedelivery'])) {
                            $lowerShippingPrice += $datum['Cost'] + $datum['CostRedelivery'];
                        } else {
                            $lowerShippingPrice += $datum['Cost'];
                        }
                    }
                }
            }
            if (isset($deliveryDate)) {
                if (isset($deliveryDate['data'])) {
                    if (isset($deliveryDate['data'][0])) {
                        if (isset($deliveryDate['data'][0]['DeliveryDate'])) {
                            if (isset($deliveryDate['data'][0]['DeliveryDate']['date'])) {
                                $lowerShippingPriceArr[$deliveryDate['data'][0]['DeliveryDate']['date']] = $lowerShippingPrice;
                            }
                        }
                    }
                }
            }
        }
        $comparedLowerPrice = INF;
        $comparedLowerDate = $this->timezone->date()->format('d-m-Y');
        if (isset($lowerShippingPriceArr)) {
            foreach ($lowerShippingPriceArr as $index => $value) {
                if ($comparedLowerPrice > $value) {
                    $comparedLowerPrice = $value;
                    $comparedLowerDate = $index;
                }
            }
            $result['price'] = $comparedLowerPrice;
            $result['date'] = $comparedLowerDate;
        }
        if (isset($result)) {
            if (!empty($this->novaposhtaHelper->getStoreConfigByCode('novaposhtashipping', 'lathing')) && $cargoType === 'Pallet') {
                if ($subtotal < $this->novaposhtaHelper->getStoreConfigByCode('novaposhtashipping', 'free_cost')) {
                    $result['price'] += count($this->optionsSeat) * $this->novaposhtaHelper->getStoreConfigByCode('novaposhtashipping', 'lathing');
                }
            }
            if (isset($result['lathing'])) {
                if (!empty($this->novaposhtaHelper->getStoreConfigByCode('novaposhtashipping', 'lathing')) && $cargoType === 'Cargo') {
                    if ($subtotal < $this->novaposhtaHelper->getStoreConfigByCode('novaposhtashipping', 'free_cost')) {
                        if (!empty($result['lathing']) && count($this->optionsSeat) < count($result['lathing'])) {
                            $result['price'] += count($this->optionsSeat) * $this->novaposhtaHelper->getStoreConfigByCode('novaposhtashipping', 'lathing');
                        } else {
                            $result['price'] += count($result['lathing']) * $this->novaposhtaHelper->getStoreConfigByCode('novaposhtashipping', 'lathing');
                        }
                    }
                }
            }
            //Бесплатная доставка при сумме товара от определенной суммы и при отсутсвии  признака товара со спешл прайсом
            if (!empty($this->novaposhtaHelper->getStoreConfigByCode('novaposhtashipping', 'free_cost')) && count($result['sale']) === 0) {
                if ($subtotal >= $this->novaposhtaHelper->getStoreConfigByCode('novaposhtashipping', 'free_cost')) {
                    $result['price'] = 0;
                }
            }
        }
        $resultCarrierData = [];
        if (isset($result)) {
            if (array_key_exists('deliveryMethod', $result)) {
                $resultCarrierData[$result['deliveryMethod']] = $result;
                return $resultCarrierData;
            }
            return $result;
        }
    }

    /**
     * @return false|\Magento\Framework\DataObject|\Perspective\NovaposhtaShipping\Model\ShippingAddress
     */
    public function getQuoteAddressClient()
    {
        return $this->shippingCheckoutAddressResourceModelCollection
            ->getItemByColumnValue('cart_id', (int)($this->getQuoteId()))
            ? $this->shippingCheckoutAddressResourceModelCollection
                ->getItemByColumnValue('cart_id', (int)($this->getQuoteId()))
            : null;
    }

    /**
     * @return mixed
     */
    public function getCityData()
    {
        if ($this->npAddressData) {
            return $this->npAddressData->getCity();
        }
    }

    /**
     * @return mixed
     */
    public function getCityLabel()
    {
        if ($this->npAddressData) {
            return $this->cityRepository->getCityByCityId($this->npAddressData->getCity())->getDescriptionUa();
        }
    }

    /**
     * @return mixed
     */
    public function getStreetData()
    {
        if ($this->npAddressData) {
            return $this->npAddressData->getStreet();
        }
    }

    /**
     * @return mixed
     */
    public function getBuildNumData()
    {
        if ($this->npAddressData) {
            return $this->npAddressData->getBuilding();
        }
    }

    /**
     * @return mixed
     */
    public function getFlat()
    {
        if ($this->npAddressData) {
            return $this->npAddressData->getFlat();
        }
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return 'c2c';
    }

    /**
     * @return mixed
     */
    public function getCitiesForFrontend()
    {
        $collectionUa = $this->cityRepository->getAllCityReturnCityId('uk_UA');
        foreach ($collectionUa as $index => $value) {
            $collection['UA_city'][] = ['label' => $value['label'], 'value' => $value['cityRef']];
        }
        return $collection;
    }

    /**
     * @param $ref
     * @return mixed
     */
    public function getCityNameByRef($ref)
    {
        return $this->cityRepository->getCityByCityRef($ref)->getDescriptionUa();
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->config->getIsEnabledConfig();
    }

    /**
     * @return int
     */
    public function getQuoteId()
    {
        return $this->getOrder()->getQuoteId();
    }

    /**
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    public function getOrder()
    {
        return $this->orderRepository->get((int)$this->request->getParam('order_id'));
    }

    /**
     * @return array|mixed|null
     */
    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    /**
     * @param array|mixed|null $deliveryDate
     */
    public function setDeliveryDate($deliveryDate)
    {
        return $this->deliveryDate[] = $deliveryDate;
    }

    /**
     * @return array|mixed|null
     */
    public function getDeiveryPrice()
    {
        return $this->deiveryPrice;
    }

    /**
     * @param array|mixed|null $deiveryPrice
     */
    public function setDeiveryPrice($deiveryPrice)
    {
        return $this->deiveryPrice[] = $deiveryPrice;
    }

    /**
     * @return array|mixed|null
     */
    public function getCounterpartyAddress()
    {
        return $this->counterpartyAddress;
    }
}
