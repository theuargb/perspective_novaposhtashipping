## Perspective Novaposhta Shipping 

### Install this package from BitBucket
To install this package from BitBucket, use the following steps:

1. Go to Magento 2 root directory.
2. Choose your version and use it in order that they written
3. For Magento 2 use following command enter the following commands:

    ```
    composer config repositories.perspective_novaposhtacatalog vcs https://bitbucket.org/monteshot/novaposhta_catalog.git
    ```

    ```
    composer require perspective/module-novaposhtacatalog:"dev-prod-2.4.3" -vvv
    ```

    ```
    composer config repositories.perspective_novaposhtashipping vcs https://bitbucket.org/monteshot/novaposhta_shipping.git
    ```

    ```
    composer require perspective/module-novaposhtashipping:"dev-release-2.4.3"   -vvv
    ```
    
4. Wait while dependencies are updated. 
5. Make an ordinary setup for the module
